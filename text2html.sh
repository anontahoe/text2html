#!/bin/sh

#   Copyright (C) 2015  anontahoe

#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


file="$1"
fileNoExt="${file%.*}"
# fyi, the file to convert should end with a newline because the "while read line" loop removes the last line


function Initialize() {
	if [[ "$file" == "" ]]; then
		exit 1
	fi

	CONFIG_PATH="/home/work/Documents/programming/scripts/text2html/"

	echo "Converting $file:"
}

function TabsToSpaces() {
	expand -t4 "$file" > "$file".expanded
}

function RemoveBBCode() {
	sed -r 's/(.*)?\[url=(.*)?\](.*)\[\/url\]/\1\3/' "$file".expanded > "$file".expanded.bbremoved_temp1
	sed -r 's/\[code\]//g' "$file".expanded.bbremoved_temp1 > "$file".expanded.bbremoved_temp2
	sed -r 's/\[\/code\]//g' "$file".expanded.bbremoved_temp2 > "$file".expanded.bbremoved_temp3
	sed -r 's/\[bold\]//g' "$file".expanded.bbremoved_temp3 > "$file".expanded.bbremoved_temp4
	sed -r 's/\[\/bold\]//g' "$file".expanded.bbremoved_temp4 > "$file".expanded.bbremoved
}

function GpgSign() {
	echo -n "enter gpg key to sign the file (optional): "
	read gpgkey
	if [[ "$gpgkey" == "" ]]; then
		return
	fi

	# set gpgext (needed in another function)
	gpgext=".asc"

	# remove bbcode tags because they're not visible on the website either
	RemoveBBCode

	gpg --sign-with "$gpgkey" --clearsign "$file".expanded.bbremoved
	grep -E "^-----BEGIN PGP SIGNATURE-----" "$file".expanded.bbremoved.asc -A200 > "$file".expanded.bbremoved.asc.extracted
	head -n3 "$file".expanded.bbremoved.asc > "$file".expanded.asc
	cat "$file".expanded "$file".expanded.bbremoved.asc.extracted >> "$file".expanded.asc
}

function ParseSpecialCharsAndBBCode() {
	sed -r 's/</\&lt;/g' "$file".expanded"$gpgext" > "$file".expanded.specialcharparsed_temp1
	sed -r 's/>/\&gt;/g' "$file".expanded.specialcharparsed_temp1 > "$file".expanded.specialcharparsed
	sed -r 's/\s/\&nbsp;/g' "$file".expanded.specialcharparsed > "$file".expanded.specialcharparsed.spaceparsed

	sed -r 's/(.*)?\[url=(.*)?\](.*)\[\/url\]/\1<a href=\2>\3<\/a>/' "$file".expanded.specialcharparsed.spaceparsed > "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp1
	sed -r 's/\[code\]/<font face=\"Courier New\">/g' "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp1 > "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp2
	sed -r 's/\[\/code\]/<\/font>/g' "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp2 > "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp3
	sed -r 's/\[bold\]/<b>/g' "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp3 > "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp4
	sed -r 's/\[\/bold\]/<\/b>/g' "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp4 > "$file".expanded.specialcharparsed.spaceparsed.bbparsed


}

function FinalizeConversion() {
	cat "$CONFIG_PATH""header.html" > "$fileNoExt".html

	# add line breaks
	while read line; do
		line="$line""<br>"
		echo "$line" >> "$fileNoExt".html
	done < "$file".expanded.specialcharparsed.spaceparsed.bbparsed

	cat "$CONFIG_PATH""footer.html" >> "$fileNoExt".html
}

function CleanUp() {
	rm "$file".expanded													&> /dev/null
	rm "$file".expanded.bbremoved										&> /dev/null
	rm "$file".expanded.bbremoved_temp1									&> /dev/null
	rm "$file".expanded.bbremoved_temp2									&> /dev/null
	rm "$file".expanded.bbremoved_temp3									&> /dev/null
	rm "$file".expanded.bbremoved_temp4									&> /dev/null
	rm "$file".expanded.bbremoved.asc									&> /dev/null
	rm "$file".expanded.bbremoved.asc.extracted							&> /dev/null
	rm "$file".expanded.asc												&> /dev/null
	rm "$file".expanded.specialcharparsed								&> /dev/null
	rm "$file".expanded.specialcharparsed_temp1							&> /dev/null
	rm "$file".expanded.specialcharparsed.spaceparsed					&> /dev/null
	rm "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp1	&> /dev/null
	rm "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp2	&> /dev/null
	rm "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp3	&> /dev/null
	rm "$file".expanded.specialcharparsed.spaceparsed.bbparsed_temp4	&> /dev/null
	rm "$file".expanded.specialcharparsed.spaceparsed.bbparsed			&> /dev/null
	rm "$file".expanded.specialcharparsed.spaceparsed.bbparsed 			&> /dev/null
}


Initialize

CleanUp

TabsToSpaces

GpgSign

ParseSpecialCharsAndBBCode

FinalizeConversion

CleanUp

exit 0
