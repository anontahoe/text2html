text2html
=========

Replaces whitespaces, line breaks and special characters with html equivalents and optionally GPG signs the website's text.  
Also adds an optional (or mandatory? can't remember) header and footer around the generated .html file.  

#### Supports the following BBcode
*	[url=example.com]example[/url]  
*	[code]changes font to courier-new[/code]  
*	[bold]bold[/bold]  

#### Supports the following special characters
*	"<"
*	">"

#### How To use  
*	Run ./text2html.sh <file-to-convert>  

#### Notes  
*	The script changes the file extension to .html, overwriting whatever file might have been there.  
*	The last line will be stripped away during parsing so add an empty line at the bottom.  
